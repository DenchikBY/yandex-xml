<?php

include __DIR__ . '/vendor/autoload.php';

class Config
{
    const inDirPath = __DIR__ . '/in_files';
    const outDirPath = __DIR__ . '/out_files';
}

class ParamsGroup
{
    /** @var string */
    public $name;

    /** @var Param[] */
    public $params;

    public function __construct(string $name, array $params = [])
    {
        $this->name = $name;
        $this->params = $params;
    }

    public function addParam(Param $param)
    {
        $this->params[] = $param;
    }
}

class Param
{
    /** @var string */
    public $name;

    /** @var string */
    public $value;

    /** @var string|null */
    public $unit;

    public function __construct(string $name, $value, $unit = null)
    {
        $this->name = $name;
        $this->value = $value;
        $this->unit = $unit;
    }

    public function render(): string
    {
        if ($this->unit != null) {
            return '<param name="' . $this->name . '" unit="' . $this->unit . '">' . $this->value . '</param>';
        } else {
            return '<param name="' . $this->name . '">' . $this->value . '</param>';
        }
    }
}

class Item
{

    /** @var PHPExcel */
    public $objPHPExcel;
    public $fileName;
    public $categoryId;
    public $categoryName;
    public $itemId;
    public $itemName;
    public $vendorName;
    public $description;
    public $pictures = [];
    public $barCodes = [];
    /** @var ParamsGroup[] $groupedParams */
    public $groupedParams = [];
    /** @var ParamsGroup[] $params */
    public $params = [];

    public function __construct(string $inFilePath, string $originalFileName)
    {
        $this->fileName = pathinfo($originalFileName, PATHINFO_FILENAME);
        $currentGroup = 0;

        $this->objPHPExcel = PHPExcel_IOFactory::load($inFilePath);
        $sheetObj = $this->objPHPExcel->getActiveSheet();

        $this->categoryId = $this->getCellValue(0, 1);
        $this->categoryName = $this->getCellValue(1, 1);
        $this->itemId = $this->getCellValue(2, 1);
        $this->itemName = $this->getCellValue(3, 1);
        $this->vendorName = $this->getCellValue(4, 1);

        foreach ($sheetObj->getRowIterator(2) as $row) {
            $rowId = $row->getRowIndex();
            $rowColor = $sheetObj->getCellByColumnAndRow(1, $rowId)->getStyle()->getFill()->getStartColor()->getRGB();
            $name = $this->getCellValue(0, $rowId);
            if ($name == 'Фото') {
                $this->pictures = $this->parsePictures($this->getCellValue(1, $rowId));
            } else if ($name == 'Дополнительная информация') {
                $this->description = $this->getCellValue(1, $rowId);
            } else if ($name == 'Код производителя') {
                $value = $this->getCellValue(1, $rowId);
                if (strlen($value) > 0) {
                    $this->barCodes[] = $value;
                }
            } else if ($rowColor != '000000') {
                $this->groupedParams[] = new ParamsGroup($name);
                $currentGroup = count($this->groupedParams) - 1;
            } else if ($rowColor = '000000') {
                $paramValue = $this->getCellValue(1, $rowId);
                list($paramName, $paramUnit) = $this->getParamData($name);
                if (array_key_exists($currentGroup, $this->groupedParams)) {
                    $this->groupedParams[$currentGroup]->addParam(new Param($paramName, $paramValue, $paramUnit));
                } else {
                    $this->params[] = new Param($paramName, $paramValue, $paramUnit);
                }
            }
        }

        //$this->description = $this->getParamsGroupByName('Дополнительно')->params[0]->value ?? '';
    }

    public function getParamData(string $paramName)
    {
        preg_match('/^(.+)\[(.+)\]$/', $paramName, $matches);
        if (count($matches) > 1) {
            return [trim($matches[1]), $matches[2]];
        } else {
            return [$paramName, null];
        }
    }

    /**
     * @param string $groupName
     * @return ParamsGroup|null
     */
    public function getParamsGroupByName(string $groupName)
    {
        $num = null;
        $i = 0;
        foreach ($this->groupedParams as $group) {
            if ($group->name == $groupName) {
                $num = $i;
                break;
            }
            ++$i;
        }
        return $num != null ? clone $this->groupedParams[$num] : null;
    }

    public function getCellValue(int $column, int $row): string
    {
        return trim($this->objPHPExcel->getActiveSheet()->getCellByColumnAndRow($column, $row)->getFormattedValue()) ?? '';
    }

    public function parsePictures(string $str): array
    {
        $str = str_replace(' ', '', $str);
        $urls = explode(',', $str);
        return $urls;
    }

}
