<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Excel to Yandex XML</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container" style="margin-top: 20px;">
    <?php if (isset($error)): ?>
    <div class="alert alert-danger" role="alert"><?=$error?></div>
    <?php endif; ?>
    <form action="/index.php?action=upload" method="post" enctype="multipart/form-data">
        <input type="file" name="file[]" class="form-control" required="required" multiple="multiple"><br>
        <button type="submit" class="btn btn-primary">Загрузить</button>
    </form>
    <?php if (isset($urls)): ?>
    <div class="alert alert-info" role="alert" style="margin-top: 20px;">
        <?php foreach($urls as $url): ?>
            <a href="<?=$url?>"><?=$url?></a><br>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
</div>
</body>
</html>