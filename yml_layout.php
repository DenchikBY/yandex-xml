<?php echo '<?xml version="1.0" encoding="UTF-8"?>';?>
<yml_catalog xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" date="2010-04-01T17:00:00" version="1.0" xsi:noNamespaceSchemaLocation="VendorYML-1.0.xsd">
    <vendor name="<?php echo $first->vendorName;?>">
        <categories>
            <category id="<?php echo $first->categoryId;?>"><?php echo $first->categoryName;?></category>
        </categories>
        <models>
            <?php foreach ($group as $item): ?>
                <model id="<?php echo $item->itemId;?>" categoryId="<?php echo $item->categoryId;?>">
                    <name><?php echo $item->itemName;?></name>
                    <?php foreach ($item->pictures as $url): ?>
                    <pictureUrl><?php echo $url;?></pictureUrl>
                    <?php endforeach; ?>
                    <?php foreach ($item->barCodes as $barCode): ?>
                        <barcode><?php echo $barCode;?></barcode>
                    <?php endforeach; ?>
                    <addDate><?php echo $date;?></addDate>
                    <updateDate><?php echo $date;?></updateDate>
                    <?php if (strlen($item->description) > 0): ?>
                    <description><?php echo $item->description;?></description>
                    <?php endif; ?>
                    <?php foreach ($item->groupedParams as $paramsGroup): ?>
                        <?php if (!in_array($paramsGroup->name, ['Код производителя', 'Дополнительно', 'Служебные поля'])): ?>
                            <?php foreach ($paramsGroup->params as $param): ?>
                                <?php echo $param->render() . PHP_EOL; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?php foreach ($item->params as $param): ?>
                        <?php echo $param->render() . PHP_EOL; ?>
                    <?php endforeach; ?>
                </model>
            <?php endforeach; ?>
        </models>
    </vendor>
</yml_catalog>