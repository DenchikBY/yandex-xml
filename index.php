<?php

ini_set('default_charset', 'UTF-8');

include __DIR__ .'/parsep.php';

function displayTemplate(array $data = []): string
{
    ob_start();
    extract($data);
    include __DIR__ . '/layout.php';
    return ob_get_clean();
}

function generateYML(array $group)
{
    ob_start();
    extract(['group' => $group, 'first' => $group[0], 'date' => (new DateTime())->format('Y-m-d')]);
    include __DIR__ . '/yml_layout.php';
    return ob_get_clean();
}

function generateFiles(array $groupedItems)
{
    $urls = [];
    foreach ($groupedItems as $categoryId => $group) {
        $xmlFileContent = generateYML($group);
        $outFileName = iconv('utf-8', 'cp1251', $group[0]->fileName . '.xml');
        file_put_contents(Config::outDirPath . '/' . $outFileName, $xmlFileContent);
        $urls[] = '/out_files/' . $outFileName;
    }
    return $urls;
}

$action = $_GET['action'] ?? null;

if ($action == 'upload') {
    $data = [];
    $groupedItems = [];
    for ($i = 0; $i < count($_FILES['file']['name']); ++$i) {
        $mime = mime_content_type($_FILES['file']['tmp_name'][$i]);
        if (!in_array($mime, ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'])) {
            $data['data'][$i]['error'] = 'Неверный формат файла';
        } else {
            $inFilePath = Config::inDirPath . DIRECTORY_SEPARATOR . md5(pathinfo($_FILES['file']['name'][$i], PATHINFO_FILENAME)) . '.xml';
            if (move_uploaded_file($_FILES['file']['tmp_name'][$i], $inFilePath)) {
                try {
                    $item = new Item($inFilePath, $_FILES['file']['name'][$i]);
                    $groupedItems[$item->categoryId][] = $item;
                } catch (Exception $exception) {
                    $data['data'][$i]['error'] = 'Ошибка обработки файла';
                }
            } else {
                $data['data'][$i]['error'] = 'Ошибка при загрузке файла';
            }
            if (file_exists($inFilePath)) unlink($inFilePath);
        }
    }
    $urls = generateFiles($groupedItems);
    $data['urls'] = $urls;
    echo displayTemplate($data);
} else {
    echo displayTemplate();
}
